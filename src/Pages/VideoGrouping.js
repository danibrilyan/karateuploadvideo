import React, {useEffect, useState} from 'react'
import {Table, TableHead, TableBody, TableCell, TableRow, TablePagination, Button, Select, MenuItem, Typography} from '@material-ui/core'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import StaticVar from "./../Config/StaticVar"
import Header from './Components/Header'
import axios from 'axios'
import _ from 'lodash'
import 'youtube-video-js';

export default function VideoGrouping(props) {
    const [video,setvideo]= useState([])

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(100);
  
    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };

    const [choose_evaluator, setchoose_evaluator] = useState("0")
    const [choose_group, setchoose_group] = useState("0")

    // useEffect(() => {        
        
    // }, [])

    return (
        <div>
            <Header history={props.history}/>
            <div style={{padding:10}}>
                <span>Cari Berdasarkan</span>
                <Select style={{marginLeft:10}} value={choose_evaluator} onChange={(e)=>setchoose_evaluator(e.target.value)}>
                    <MenuItem value="0">Semua Penguji</MenuItem>
                    <MenuItem value="EVA_006">Erza</MenuItem>
                    <MenuItem value="EVA_008">Fauzi</MenuItem>
                </Select>
                <Select style={{marginLeft:10}} value={choose_group} onChange={(e)=>setchoose_group(e.target.value)}>
                    <MenuItem value="0">Pilih Group</MenuItem>
                    {
                        [1,2,3,4,5,6,7,8,9,10,
                            11,12,13,14,15,16,17,18,19,20,
                            21,22,23,24,25,26,27,28,29,30,
                            31,32,33,34,35,36,37,38,39,40,
                            41,42,43,44,45,46,47,48,49,50,
                            51,52,53,54,55,56,57,58,59,60,
                            61,62,63,64,65,66,67,68,69,70,
                            71,72,73,74,75,76,77,78,79,80,
                            81,82,83,84,85,86,87,88,89,90].map(item=>{
                                return(
                                    <MenuItem value={item}>GROUP - {item}</MenuItem>
                                )
                            })
                    }
                </Select>
                <Button variant="contained" color="primary" onClick={()=>{
                    axios.get(StaticVar.URL_API+'/peserta/videogrouping?event_id='+StaticVar.EVENT_ID+'&EVALUATOR_ID='+choose_evaluator+'&GROUP_ID='+choose_group).then(res=>{
                        var result = _.chain(_.orderBy(res.data,["KYU"], ["asc"]))
                                    .groupBy("VIDEO_ID")
                                    .map((value, key) => {
                                        return {
                                            _id: key, 
                                            data: value,
                                            length:value.length
                                        }
                                    })
                                    .value();
                        console.log('b', JSON.stringify(result))
                        setvideo(result)
                    })
                }}>Cari</Button>
            </div>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell style={{width:20}}>
                            No
                        </TableCell>
                        <TableCell style={{width:300}}>
                            Video
                        </TableCell>
                        <TableCell>
                            Peserta
                        </TableCell>
                        <TableCell style={{width:300}}>
                            Group
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        _.orderBy(video,['length'],["desc"])
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((itemvideo,index)=>{
                            var datasingle = itemvideo.data.length>0?itemvideo.data[0]:{}
                            return(
                                <TableRow>
                                    <TableCell>
                                        {
                                            page*rowsPerPage + index+1
                                        }
                                    </TableCell>
                                    <TableCell>
                                        <Card>
                                            <CardActionArea>
                                                {/* <youtube-video 
                                                    height="160"
                                                    width="325"
                                                    src={itemvideo._id}
                                                    controls
                                                /> */}
                                                <a href={itemvideo._id} target="_blank"><div style={{padding:5}}>{itemvideo._id}</div></a>
                                            </CardActionArea>
                                        </Card>
                                    </TableCell>

                                    <TableCell>
                                        <Table>
                                            {
                                                itemvideo.data.map(itemparticipant=>{
                                                    return(
                                                        <TableRow>
                                                            <TableCell style={{width:300}}>{itemparticipant.DOJO_NAME}</TableCell>
                                                            <TableCell>{itemparticipant.PARTICIPANT_NAME}</TableCell>
                                                            <TableCell style={{width:100}}>{itemparticipant.KYU}</TableCell>
                                                        </TableRow>
                                                    )
                                                })
                                            }
                                        </Table>
                                    </TableCell>
                                    <TableCell>
                                        {
                                           "GROUP "+ datasingle.GROUP_ID
                                        }
                                        
                                    </TableCell>
                                    <TableCell>
                                        <Button variant="contained" color="primary" onClick={()=>props.history.push('/penilaian?evaluator_id='+choose_evaluator+'&belt_id='+datasingle.BELT_ID+'&group_id='+datasingle.GROUP_ID)}>Buka</Button>
                                    </TableCell>
                                </TableRow>
                            )
                        })
                    }
                    
                </TableBody>
            </Table>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50, 100, 150, 250, 300]}
                component="div"
                count={video.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    )
}
