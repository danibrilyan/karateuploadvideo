import React, {useState, useEffect} from 'react'
import {Button, AppBar, Toolbar, Grid, Typography, IconButton, Table, TableHead, TableRow, TableCell, TableBody, Checkbox, TextField} from '@material-ui/core'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import sampleimage from './../sampleimage.jpg'
import DialogFull from "./Components/DialogFullScreen"
import StaticVar from "./../Config/StaticVar"
import axios from 'axios'
import Header from './Components/Header'
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    container:{
        padding:20
    },
    media: {
        height: 100,
    },
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: "#CFCFCF",
      color: theme.palette.common.black,
    },
    body: {
      fontSize: 14,
    },
}))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
}))(TableRow);

export default function Home(props) {
    const classes = useStyles();
    const [openModal, setOpenModal] = useState(false)
    const handleClose = ()=>{
        setOpenModal(false)
    }
    const openUploadVideo =(item)=>{
        setuploadbelt(item)
        setcheckParticipant([])
        setOpenModal(true)
    }

    const params = new URLSearchParams(window.location.search);
    const dojo_name = params.get("dojo_name");
    const [video_link, setvideo_link] = useState("")
    const [pesertaupload, setpesertaupload] = useState([])
    const [videopeserta, setvideopeserta] = useState([])
    const [uploadbelt, setuploadbelt] = useState({})
    const [checkParticipant, setcheckParticipant] = useState([])
    const [createFolderStatus, setcreateFolderStatus] = useState(false)
    const [kelompokpenguji, setkelompokpenguji] = useState([])
    const sabuk =[{'BELT_ID':'BELT-1','BELT_NAME':'Sabuk Putih'},
        {'BELT_ID':'BELT-2', 'BELT_NAME':'Sabuk Kuning I'},
        {'BELT_ID':'BELT-22', 'BELT_NAME':'Sabuk Kuning II'},
        {'BELT_ID':'BELT-3', 'BELT_NAME':'Sabuk Hijau I'},
        {'BELT_ID':'BELT-32', 'BELT_NAME':'Sabuk Hijau II'},
        {'BELT_ID':'BELT-4', 'BELT_NAME':'Sabuk Biru I'},
        {'BELT_ID':'BELT-42', 'BELT_NAME':'Sabuk Biru II'},
        {'BELT_ID':'BELT-5', 'BELT_NAME':'Sabuk Cokelat'}]
    useEffect(() => {
        if(!dojo_name){
            props.history.push('/dojo')
        }
        getdatavideo()
    }, [])

    const getdatavideo = ()=>{
        axios.get(StaticVar.URL_API+'/peserta/upload?event_id='+StaticVar.EVENT_ID+'&dojo_name='+dojo_name).then(res=>{
            console.log('upload', res.data)
            setpesertaupload(res.data)
        })
        axios.get(StaticVar.URL_API+'/peserta/video?event_id='+StaticVar.EVENT_ID+'&dojo_name='+dojo_name).then(res=>{
            console.log('video', res.data)
            setvideopeserta(res.data)
        })

        if(localStorage.user){
            const user = JSON.parse(localStorage.user)
            axios.get(StaticVar.URL_API+'/penguji/kelompok?event_id='+StaticVar.EVENT_ID+'&evaluator_id='+user.EVALUATOR_ID+'&dojo_name='+dojo_name).then(respeserta=>{
                console.log('KELOMPOK', respeserta.data)
                setkelompokpenguji(respeserta.data)
            })
        }
        
        

    }

    const registervideoexam = (id, name) =>{
        axios.post(StaticVar.URL_API+'/peserta/upload',{
            event_id:StaticVar.EVENT_ID,
            video_id:id, video_name:name, participant: checkParticipant, 
            belt_id: uploadbelt.BELT_ID, belt_name: uploadbelt.BELT_NAME, dojo_id:'12345', dojo_name: dojo_name
        })
    }

    const updatevideoexam = async(id) =>{
        checkParticipant.map(async item=>{
            await axios.post(StaticVar.URL_API+'/peserta/update',{event_id:StaticVar.EVENT_ID,video_id:id,  participant_id: item})
        })
        setTimeout(() => {
            setOpenModal(false);
            getdatavideo()   
            setcreateFolderStatus(false)
        }, 3000);
    }

    const updatelink = async()=>{
        // registervideoexam("Youtube", res.data.namaFolder)
        // updatevideoexam(res.data.idFolder)
    }

    const createFolder = async()=>{
        console.log('cek', checkParticipant)
        console.log('data', pesertaupload)
        checkParticipant.forEach(async x=>{
            await axios.post(StaticVar.URL_API+'/peserta/update',{event_id:StaticVar.EVENT_ID,video_id:x.VIDEO_ID,  participant_id: x.PARTICIPANT_ID})
        })
        
        setTimeout(() => {
            setOpenModal(false);
            getdatavideo()   
            setcreateFolderStatus(false)
        }, 500);

        // axios.post(StaticVar.URL_API+'/peserta/upload',{
        //     video_id:id, video_name:name, participant: checkParticipant, 
        //     belt_id: uploadbelt.BELT_ID, belt_name: uploadbelt.BELT_NAME, dojo_id:'12345', dojo_name: dojo_name
        // })
        // registervideoexam(res.data.idFolder, res.data.namaFolder)
        // updatevideoexam(res.data.idFolder)
        //setcreateFolderStatus(true);
        // const checkfolderutama = await axios.get(StaticVar.URI_GDRIVE+'/file/listFolderById/'+StaticVar.KEY_FOLDER).then(res=>res.data)
        // if(checkfolderutama.length>0){ // Jika Folder Exam Tersedia
        //     console.log('checkfolderutama', checkfolderutama)
        //     if(checkfolderutama !== "folder kosong")
        //     {
        //         const checkfolderdojo = checkfolderutama.filter(x=>x.name === dojo_name)
        //         if(checkfolderdojo.length>0){ // Jika folder
        //             console.log('folder dojo', checkfolderdojo)
        //             const folderdojo = await axios.get(StaticVar.URI_GDRIVE+'/file/listFolderById/'+checkfolderdojo[0].id).then(res=>res.data)
        //             const checkbeltfolder = folderdojo.filter(x=>x.name===uploadbelt.BELT_NAME)
        //             if(checkbeltfolder.length>0){
        //                 console.log('folder sabuk tersedia', checkbeltfolder)
        //                 const foldername = 'Kelompok '+ (checkbeltfolder.length)+1
        //                 axios.post(StaticVar.URI_GDRIVE+'/file/'+checkbeltfolder[0].id, {folder:foldername}).then(res=>{
        //                     console.log('res.data', res.data)
        //                     registervideoexam(res.data.idFolder, res.data.namaFolder)
        //                     updatevideoexam(res.data.idFolder)
        //                 })
        //             }
        //             else{
        //                 //buat folder sabuk
        //                 console.log('folder sabuk tidak tersedia', checkbeltfolder)
        //                 axios.post(StaticVar.URI_GDRIVE+'/file/'+checkfolderdojo[0].id, {folder:uploadbelt.BELT_NAME}).then(res=>{
        //                     //buat folder kelompok 
        //                     const foldername = 'Kelompok 1'
        //                     axios.post(StaticVar.URI_GDRIVE+'/file/'+res.data.idFolder, {folder:foldername}).then(reschild=>{
        //                         console.log('res.data', reschild.data)
        //                         registervideoexam(reschild.data.idFolder, reschild.data.namaFolder)
        //                         updatevideoexam(reschild.data.idFolder)
        //                     })
        //                 })
        //             }
                    
        //         }
        //         else{
        //             console.log('tidak ada folder dojo', checkfolderdojo)
        //             //buat folder dojo
        //             axios.post(StaticVar.URI_GDRIVE+'/file/'+StaticVar.KEY_FOLDER, {folder:dojo_name}).then(resdojo=>{
        //                 console.log('res.data', resdojo.data)
        //                 //buat folder sabuk
        //                 axios.post(StaticVar.URI_GDRIVE+'/file/'+resdojo.data.idFolder, {folder:uploadbelt.BELT_NAME}).then(resbelt=>{
        //                     //buat folder kelompok 
        //                     const foldername = 'Kelompok 1'
        //                     axios.post(StaticVar.URI_GDRIVE+'/file/'+resbelt.data.idFolder, {folder:foldername}).then(reschild=>{
        //                         console.log('res.data', reschild.data)
        //                         registervideoexam(reschild.data.idFolder, reschild.data.namaFolder)
        //                         updatevideoexam(reschild.data.idFolder)
        //                     })
        //                 })
        //             })
        //         }
        //     }
        //     else{
        //             //buat folder dojo
        //         axios.post(StaticVar.URI_GDRIVE+'/file/'+StaticVar.KEY_FOLDER, {folder:dojo_name}).then(resdojo=>{
        //             console.log('res.data', resdojo.data)
        //             //buat folder sabuk
        //             axios.post(StaticVar.URI_GDRIVE+'/file/'+resdojo.data.idFolder, {folder:uploadbelt.BELT_NAME}).then(resbelt=>{
        //                 //buat folder kelompok 
        //                 const foldername = 'Kelompok 1'
        //                 axios.post(StaticVar.URI_GDRIVE+'/file/'+resbelt.data.idFolder, {folder:foldername}).then(reschild=>{
        //                     console.log('res.data', reschild.data)
        //                     registervideoexam(reschild.data.idFolder, reschild.data.namaFolder)
        //                     updatevideoexam(reschild.data.idFolder)
        //                 })
        //             })
        //         })
        //     }
            
        // }
        // else{
        //     console.log('tidak ada folder utama', checkfolderutama)
        // }
    }
    return (
        <div className={classes.root}>
            <DialogFull
                open={openModal}
                close={handleClose}
                title={"Upload Video"}
                content={
                    <div style={{marginTop:70}}>
                        <Typography>Silahkan Pilih Nama Peserta</Typography>
                        <Table size="small" style={{marginBottom:10}}>
                            <TableHead  color="primary" >
                                <TableRow>
                                    <StyledTableCell style={{width:20,textAlign:'center'}}>No</StyledTableCell>
                                    <StyledTableCell>Nama</StyledTableCell>
                                    <StyledTableCell style={{width:120,textAlign:'center'}}>Kyu</StyledTableCell>
                                    <StyledTableCell style={{width:320,textAlign:'center'}}>Video</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    pesertaupload.filter(x=>x.BELT_ID===uploadbelt.BELT_ID).map((item,index)=>{
                                        const itemcheck = checkParticipant.filter(x=>x.PARTICIPANT_ID === item.PARTICIPANT_ID).length>0? true : false
                                        //const VIDEO_ID = checkParticipant.filter(x=>x.PARTICIPANT_ID === item.PARTICIPANT_ID).length>0? checkParticipant.filter(x=>x.PARTICIPANT_ID === item.PARTICIPANT_ID)[0] : ''
                                        return(
                                            <TableRow>
                                                <StyledTableCell style={{textAlign:'center'}}>
                                                    {/* {
                                                        item.VIDEO_ID ? (index+1) :  */}
                                                        <Checkbox checked={itemcheck} onChange={(event)=>{
                                                            let check = checkParticipant;
                                                            if(event.target.checked ){
                                                                check = [...check, {PARTICIPANT_ID: item.PARTICIPANT_ID, VIDEO_ID:item.VIDEO_ID}]
                                                            }
                                                            else{
                                                                check = check.filter(x=>x.PARTICIPANT_ID !== item.PARTICIPANT_ID)
                                                            }
                                                            setcheckParticipant(check)
                                                        }} />
                                                    {/* } */}
                                                </StyledTableCell>
                                                <StyledTableCell>{item.PARTICIPANT_NAME}</StyledTableCell>
                                                <StyledTableCell style={{textAlign:'center'}}>{item.KYU}</StyledTableCell>
                                                <StyledTableCell style={{textAlign:'center'}}>
                                                    {
                                                        itemcheck ? 
                                                        <TextField value={item.VIDEO_ID} style={{width:'100%'}} onChange={e=>{
                                                            var datachange = checkParticipant.map(x=>{
                                                                console.log('x', x.PARTICIPANT_ID)
                                                                console.log('item', item.PARTICIPANT_ID)
                                                                console.log('VIDEO_ID', e.target.value)
                                                                if(x.PARTICIPANT_ID === item.PARTICIPANT_ID){
                                                                    return {...x, VIDEO_ID:e.target.value}
                                                                }
                                                                else{
                                                                    return x
                                                                }
                                                            })
                                                            var datachange2 = pesertaupload.map(x=>{
                                                                console.log('x', x.PARTICIPANT_ID)
                                                                console.log('item', item.PARTICIPANT_ID)
                                                                console.log('VIDEO_ID', e.target.value)
                                                                if(x.PARTICIPANT_ID === item.PARTICIPANT_ID){
                                                                    return {...x, VIDEO_ID:e.target.value}
                                                                }
                                                                else{
                                                                    return x
                                                                }
                                                            })
                                                            console.log('datachange', datachange)
                                                            setcheckParticipant([...datachange])
                                                            setpesertaupload([...datachange2])
                                                        }}/> :
                                                        <Typography>{item.VIDEO_ID}</Typography>
                                                    }
                                                    
                                                </StyledTableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                                
                            </TableBody>
                        </Table>
                        
                        {
                            checkParticipant.length>0 ? 
                            <div style={{flex:1, border:"1px solid #CFCFCF", marginTop:20, padding:20}}>
                                {/* <Typography variant="h5">Nama Didalam Video </Typography>
                                {
                                    checkParticipant.map(x=>{
                                        return pesertaupload.filter(y=>y.PARTICIPANT_ID === x).map((item,index)=>{
                                            return(
                                                <Typography>{item.PARTICIPANT_NAME}</Typography>
                                            )
                                        })
                                    })
                                } */}
                                <Typography>
                                    {/* <TextField value={video_link} placeholder="Masukkan Link Youtube Kelompok Ini" style={{width:'100%', marginBottom:5}} onChange={e=>setvideo_link(e.target.value)} /> */}
                                    <Button variant="contained" color="primary" onClick={createFolder}>Simpan Link</Button>
                                </Typography>
                                {/* {
                                    createFolderStatus ? 
                                    <Button variant="contained" color="default">Sedang Membuat Folder</Button> :
                                    <Button variant="contained" color="primary" onClick={createFolder}>Buat Folder</Button>
                                } */}
                                
                            </div> : null
                        }
                        
                        
                    </div>
                }
                    cancel={handleClose}
                    valueCancel={"Tutup"}
                    colorButtonConfirm={"#bf272b"}
            />

            <Header history={props.history}/>
            <div className={classes.container}>
                <Typography style={{marginBottom:10}}>Nama Dojo : {dojo_name}</Typography>
            {
                sabuk.map((item, index)=>{
                    return(
                        <>
                        {
                            pesertaupload.filter(x=>x.BELT_ID===item.BELT_ID).length >0 ?
                            <div style={{marginBottom:20}}>
                                <div style={{flex:1, backgroundColor:'#CFCFCF', padding:5}}>
                                    <Grid container>
                                        <Grid item md={6}>
                                            <Typography>{item.BELT_NAME}</Typography>
                                        </Grid>
                                        <Grid item md={6}>
                                            <Typography style={{textAlign:'right'}}>Jumlah Peserta / Video : {pesertaupload.filter(x=>x.BELT_ID===item.BELT_ID).length} / {pesertaupload.filter(x=>x.BELT_ID===item.BELT_ID && (x.VIDEO_ID !== null && x.VIDEO_ID !== "")).length} orang</Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div style={{flex:1, padding:5}}>
                                    {
                                        !localStorage.user ?  
                                    <Button variant="contained" onClick={()=> openUploadVideo(item)} color="primary">Upload Video</Button> : null }
                                    <Grid container spacing={3} style={{marginTop:1}}>
                                        {
                                            videopeserta.filter(x=>x.BELT_ID===item.BELT_ID).map(itemvideo=>{
                                                const datakelompok = kelompokpenguji.filter(x=>x.VIDEO_ID === itemvideo.VIDEO_ID)
                                                const jumlahkelompok = localStorage.user ? datakelompok.length : 1
                                                return(
                                                    <>
                                                        {
                                                            jumlahkelompok > 0 ?
                                                            <Grid item md={3}>
                                                        <Card >
                                                            <CardActionArea>
                                                                <CardContent>
                                                                <Typography variant="body2" color="textSecondary" component="p">
                                                                    <ul style={{padding:0, margin:3}}>
                                                                        {JSON.parse(itemvideo.PARTICIPANT).map(itemvideo=>{
                                                                            return pesertaupload.filter(x=>x.PARTICIPANT_ID===itemvideo).map(item=>{
                                                                                return <li>{item.PARTICIPANT_NAME}</li>
                                                                            })
                                                                        })}
                                                                    </ul>
                                                                </Typography>
                                                                </CardContent>
                                                            </CardActionArea>
                                                            <CardActions>
                                                                
                                                                {
                                                                    localStorage.user ?                                                             
                                                                <Button style={{flex:1}} variant="contained" size="small" color="primary" onClick={()=>{
                                                                    props.history.push('/Penilaian?video_id='+itemvideo.VIDEO_ID+'&belt_id='+item.BELT_ID+'&group_id='+datakelompok[0].GROUP_ID)
                                                                }}>
                                                                    Nilai
                                                                </Button> : 
                                                                <Button style={{flex:1}} variant="contained" size="small" color="primary" onClick={()=>{
                                                                    window.open('https://drive.google.com/drive/u/3/folders/'+itemvideo.VIDEO_ID)
                                                                }}>
                                                                    Lihat
                                                                </Button> }
                                                            </CardActions>
                                                        </Card>
                                                    </Grid>
                                                        :
                                                            <></>
                                                        }
                                                    </>
                                                )
                                            })
                                        }
                                        
                                    </Grid>
                                </div>
                            </div>: null
                        }
                        
                        </>
                    )
                })
                }
                
            </div>
            
        </div>
    )
}
