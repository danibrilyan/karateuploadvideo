import React, {useState, useEffect} from 'react'
import {Button, AppBar, Toolbar, Grid, TextField, Typography, IconButton, Table, TableHead, TableRow, TableCell, TableBody, Checkbox} from '@material-ui/core'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import DialogModal from "./Dialog"
import axios from 'axios'
import StaticVar from "./../../Config/StaticVar"

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    container:{
        padding:20
    },
    media: {
        height: 100,
    },
}));

export default function Header(props) {
    const classes = useStyles();
    const [openModal, setopenModal] = useState(false)
    const [username, setusername] = useState('')
    const [password, setpassword] = useState('')
    const handleClose = ()=>{
        setopenModal(false)
    }

    useEffect(() => {
        
    }, [])
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    Karate Examination - Virtual
                </Typography>
                {
                    localStorage.user ? 
                    <>
                        
                        <Button color="inherit" onClick={()=>{localStorage.removeItem('user');window.location.reload() }}><Typography>Hi, {JSON.parse(localStorage.user).EVALUATOR_NAME} </Typography></Button>
                    </> : 
                    <Button color="inherit" onClick={()=>{setopenModal(true)}}>Login</Button> 
                }
                <Button color="inherit" onClick={()=>props.history.goBack()}>Kembali</Button>
            </Toolbar>
            <DialogModal
                open={openModal}
                close={handleClose}
                title={"Login Penguji"}
                content={
                    <>
                    <TextField id="standard-basic" label="User Name" value={username} onChange={(e)=>setusername(e.target.value)} />
                    <br/>
                    <TextField id="standard-basic" label="Password" value={password} onChange={(e)=>setpassword(e.target.value)} />
                    <br/>
                    <Button variant="contained" color="primary" onClick={()=>{
                        axios.post(StaticVar.URL_API+'/login/', {event_id: StaticVar.EVENT_ID, username, password}).then(res=>{
                            console.log('login', res.data)
                            if(res.data.length>0){
                                const user = {"EVALUATOR_ID":res.data[0].EVALUATOR_ID, "EVALUATOR_NAME":res.data[0].EVALUATOR_NAME}
                                localStorage.setItem('user', JSON.stringify(user));
                                handleClose()
                                // props.history.push('/dojo')
                                window.location.reload()
                                
                            }
                            else{
                                alert('Login Tidak sesuai')
                            }
                        })
                        
                    }} style={{marginTop:20}}>Login</Button>
                    </>
                }
                cancel={handleClose}
                valueCancel={"Tutup"}
                colorButtonConfirm={"#bf272b"}
            />
        </AppBar>
    )
}
