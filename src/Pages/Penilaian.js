import React, {useState, useEffect} from 'react'
import Header from './Components/Header'
import {Button, CardMedia, AppBar, Toolbar, Grid, Typography, TextField , IconButton, Table, TableHead, TableRow, TableCell, TableBody, Checkbox, Select, MenuItem} from '@material-ui/core'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import axios from 'axios';
import StaticVar from "./../Config/StaticVar"
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import DialogModal from "./Components/Dialog"

export default function Penilaian(props) {
    const params = new URLSearchParams(window.location.search);
    //const video_id = params.get("video_id");
    const evaluator_id = params.get("evaluator_id");
    const group_id = params.get("group_id");
    const belt_id = params.get("belt_id");
    const [video, setvideo] = useState([])
    const [video_exam, setvideo_exam] = useState("")
    const [detailinfo, setdetailinfo] = useState([])
    const [pesertainfo, setpesertainfo] = useState([])
    const [materiujian, setmateriujian] = useState([])
    const [pilihmateri, setpilihmateri] = useState({})
    const [materiheader, setmateriheader] = useState('')
    const [assheader, setassheader] = useState('')
    const [choose_group, setchoose_group] = useState("0")
    const [materipeserta, setmateripeserta] = useState([       
    ])

    const [progres, setprogres] = useState([])

    const submitNilai = async()=> {
        const datanilai = materipeserta.filter(x=>x.ASSESMENT_ID === pilihmateri.ASSESMENT_ID)
        console.log('datanilai', datanilai)
        datanilai.map(x=>{
            axios.post(StaticVar.URL_API+'/nilai', {event_id:StaticVar.EVENT_ID,score : x.SCORE, assesment_id: x.ASSESMENT_ID, participant_id : x.PARTICIPANT_ID}).then(res=>{
                console.log('res', res.data)
            })
        })
        await getprogress();
        alert('Data Telah dikirimkan')    
           
    }

    const getprogress = () =>{
        //const user = JSON.parse(localStorage.user)
        axios.get(StaticVar.URL_API+'/progres?event_id='+StaticVar.EVENT_ID+'&evaluator_id='+evaluator_id+'&group_id='+choose_group).then(res=>{
            console.log('progres', res.data)
            setprogres(res.data)
        })
    }
    
    
    useEffect(() => {
        // if(!localStorage.user){
        //     setopenModal(true)
        // }
        // axios.get(StaticVar.URI_GDRIVE+'/file/listFolderById/'+video_id).then(res=>setvideo(res.data))
        
        // axios.get(StaticVar.URL_API+'/peserta/upload?video_id='+video_id).then(res=>{
        //     console.log('peserta', res.data)
        //     setpesertainfo(res.data)
        // })
        setchoose_group(group_id)
        getprogress()

        axios.get(StaticVar.URL_API+'/peserta/videogrouping?event_id='+StaticVar.EVENT_ID+'&EVALUATOR_ID='+evaluator_id+'&GROUP_ID='+group_id).then(res=>{
            console.log('peserta', res.data)
            setpesertainfo(res.data)
            setvideo_exam(res.data.length>0?res.data[0].VIDEO_ID:"")
        })

        axios.get(StaticVar.URL_API+'/peserta/materi?event_id='+StaticVar.EVENT_ID+'&belt_id='+belt_id).then(res=>{
            console.log('materiujian', res.data)
            setmateriujian(res.data)
        })

        axios.get(StaticVar.URL_API+'/peserta/penilaian?event_id='+StaticVar.EVENT_ID+'&evaluator_id='+evaluator_id+'&group_id='+group_id).then(res=>{
            console.log('materipeserta', res.data)
            setmateripeserta(res.data)
        })
    }, [])
    return (
        <div>
             
            <Header history={props.history}/>
            
            {/* {
                localStorage.user ?  */}
                <div style={{marginTop:10, flex:1, padding:5}}> 
                    <Typography>Nama Penguji : {evaluator_id === "EVA_006" ? "Erza": "Fauzi"}</Typography>
                    <Typography>
                        Pilih Group : 
                        <Select value={choose_group} onChange={e=>setchoose_group(e.target.value)}>
                            <MenuItem value="0">Pilih Group</MenuItem>
                            {
                                [1,2,3,4,5,6,7,8,9,10,
                                    11,12,13,14,15,16,17,18,19,20,
                                    21,22,23,24,25,26,27,28,29,30,
                                    31,32,33,34,35,36,37,38,39,40,
                                    41,42,43,44,45,46,47,48,49,50,
                                    51,52,53,54,55,56,57,58,59,60,
                                    61,62,63,64,65,66,67,68,69,70,
                                    71,72,73,74,75,76,77,78,79,80,
                                    81,82,83,84,85,86,87,88,89,90].map(item=>{
                                        return(
                                            <MenuItem value={item}>GROUP - {item}</MenuItem>
                                        )
                                    })
                            }
                        </Select>
                        <Button variant="contained" color="primary" onClick={()=>{
                            // getprogress()

                            // axios.get(StaticVar.URL_API+'/peserta/videogrouping?EVALUATOR_ID='+evaluator_id+'&GROUP_ID='+choose_group).then(res=>{
                            //     console.log('peserta', res.data)
                            //     setpesertainfo(res.data)
                            //     setvideo_exam(res.data.length>0?res.data[0].VIDEO_ID:"")
                            // })
                    
                            
                    
                            // axios.get(StaticVar.URL_API+'/peserta/penilaian?evaluator_id='+evaluator_id+'&group_id='+choose_group).then(res=>{
                            //     console.log('materipeserta', res.data)
                            //     setmateripeserta(res.data)
                            // })
                            //props.history.push('/penilaian?evaluator_id='+evaluator_id+'&belt_id='+belt_id+'&group_id='+choose_group)
                            axios.get(StaticVar.URL_API+'/peserta/videogrouping?event_id='+StaticVar.EVENT_ID+'&EVALUATOR_ID='+evaluator_id+'&GROUP_ID='+choose_group).then(res=>{
                                console.log('res', res.data)
                                if(res.data.length>0){
                                    window.location.assign('/penilaian?evaluator_id='+evaluator_id+'&belt_id='+res.data[0].BELT_ID+'&group_id='+choose_group)
                                    axios.get(StaticVar.URL_API+'/peserta/materi?event_id='+StaticVar.EVENT_ID+'&belt_id='+res.data[0].BELT_ID).then(res=>{
                                        console.log('materiujian', res.data)
                                        setmateriujian(res.data)
                                    })
                                }
                                
                            })
                            
                        }}>Cari</Button>
                    </Typography>
                    <center>
                        <Card>
                            <CardActionArea>
                                <youtube-video
                                    height="240"
                                    width="350"
                                    src={video_exam}
                                    controls
                                />
                            </CardActionArea>
                        </Card>
                    </center>             
                    <Grid container spacing={3}>                                        
                        <Grid item md={4} sm={12} xs={12}>
                            <p>Materi Penilaian</p>
                            <Button variant="contained" color={materiheader === "KIHON" ? "primary": "default"} onClick={()=>{setmateriheader("KIHON"); setassheader("ASS-01")}} style={{width:'48%', marginRight:10}}>KIHON</Button>
                            <Button variant="contained" color={materiheader === "KATA" ? "primary": "default"} onClick={()=>{setmateriheader("KATA"); setassheader("ASS-02")}} style={{width:'48%'}}>KATA</Button>
                            <List align="center">
                                { materiujian.filter(x=>x.PARENT_ID === assheader).map((item,index)=>{
                                    let datamateri = materipeserta.filter(x=>x.ASSESMENT_ID === item.ASSESMENT_ID && x.SCORE > 0)
                                    return(
                                        <div style={{border:"1px solid #CFCFCF", flex:1, backgroundColor:datamateri.length>0 ?'limegreen': 'salmon'}}>
                                            <ListItemText button="true" onClick={()=>setpilihmateri(item)} primary={item.ASSESMENT_NAME}/>                                   
                                        </div>
                                    )
                                })}
                                
                            </List>
                            <div style={{border:"1px solid #CFCFCF", flex:1, padding:10}}>
                                <Typography variant="h4">Progres : {
                                    progres.map(x=>{
                                        return Math.floor(x.SUDAH_DINILAI/(x.SUDAH_DINILAI+x.BELUM_DINILAI)*100)
                                    })
                                } %</Typography>                              
                            </div>
                        </Grid>
                        <Grid item md={8} sm={12} xs={12}>
                            <div style={{border:"1px solid #CFCFCF", backgroundColor:'limegreen', flex:1, padding:10, marginTop:20}}>
                                <Typography variant="h4" align="center">{pilihmateri.ASSESMENT_NAME}</Typography>
                            </div>
                            <List>
                                {
                                    pesertainfo.map((item,index)=>{
                                        let datamateri = materipeserta.filter(x=>x.PARTICIPANT_ID === item.PARTICIPANT_ID && x.ASSESMENT_ID === pilihmateri.ASSESMENT_ID)
                                        let scorepeserta = datamateri.length>0 ? datamateri[0].SCORE : 0
                                        return(
                                            <>
                                            <ListItem>
                                                <ListItemAvatar>
                                                <Avatar>
                                                    <ImageIcon />
                                                </Avatar>
                                                </ListItemAvatar>
                                                <ListItemText primary={item.PARTICIPANT_NAME} secondary={item.KYU}/>
                                                <div>
                                                    <Button style={{marginRight:5}} variant="contained" color={scorepeserta == 20 ? "primary": "default"} onClick={()=>{
                                                        const datamateri = materipeserta.map(x=>{
                                                            if(x.PARTICIPANT_ID === item.PARTICIPANT_ID && x.ASSESMENT_ID === pilihmateri.ASSESMENT_ID){
                                                                return {...x, ASSESSMENT_STATUS: true, SCORE: 20}
                                                            }
                                                            else{
                                                                return x
                                                            }
                                                        })
                                                        
                                                        setmateripeserta(datamateri)
                                                    }}>KS</Button>
                                                    <Button style={{marginRight:5}} variant="contained" color={scorepeserta == 40 ? "primary": "default"} onClick={()=>{
                                                        const datamateri = materipeserta.map(x=>{
                                                            if(x.PARTICIPANT_ID === item.PARTICIPANT_ID && x.ASSESMENT_ID === pilihmateri.ASSESMENT_ID){
                                                                return {...x, ASSESSMENT_STATUS: true,SCORE: 40}
                                                            }
                                                            else{
                                                                return x
                                                            }
                                                        })
                                                        setmateripeserta(datamateri)
                                                    }}>K</Button>
                                                    <Button style={{marginRight:5}} variant="contained" color={scorepeserta == 60 ? "primary": "default"} onClick={()=>{
                                                        const datamateri = materipeserta.map(x=>{
                                                            if(x.PARTICIPANT_ID === item.PARTICIPANT_ID && x.ASSESMENT_ID === pilihmateri.ASSESMENT_ID){
                                                                return {...x, ASSESSMENT_STATUS: true,SCORE: 60}
                                                            }
                                                            else{
                                                                return x
                                                            }
                                                        })
                                                        setmateripeserta(datamateri)
                                                    }}>C</Button>
                                                    <Button style={{marginRight:5}} variant="contained" color={scorepeserta == 80 ? "primary": "default"} onClick={()=>{
                                                        const datamateri = materipeserta.map(x=>{
                                                            if(x.PARTICIPANT_ID === item.PARTICIPANT_ID && x.ASSESMENT_ID === pilihmateri.ASSESMENT_ID){
                                                                return {...x, ASSESSMENT_STATUS: true,SCORE: 80}
                                                            }
                                                            else{
                                                                return x
                                                            }
                                                        })
                                                        setmateripeserta(datamateri)
                                                    }}>B</Button>
                                                    <Button style={{marginRight:5}} variant="contained" color={scorepeserta == 100 ? "primary": "default"} onClick={()=>{
                                                        const datamateri = materipeserta.map(x=>{
                                                            if(x.PARTICIPANT_ID === item.PARTICIPANT_ID && x.ASSESMENT_ID === pilihmateri.ASSESMENT_ID){
                                                                return {...x, ASSESSMENT_STATUS: true,SCORE: 100}
                                                            }
                                                            else{
                                                                return x
                                                            }
                                                        })
                                                        setmateripeserta(datamateri)
                                                    }}>BS</Button>
                                                </div>
                                            </ListItem>
                                            <hr/>
                                            </>
                                        )
                                    })
                                }
                                
                            </List>
                            
                            <Button variant="contained" color="primary" onClick={()=> submitNilai()}>Submit Nilai</Button>
                        </Grid>
                    </Grid>
                </div>
                 {/* : 
                <div style={{marginTop:10, flex:1, padding:5}}> 
                    <center style={{marginTop:100}}>
                        <p>Silahkan Login Terlebih dahulu</p>
                    </center>
                </div> */}
            {/* } */}
            
        </div>
    )
}
