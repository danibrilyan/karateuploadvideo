import { Typography,Table, TableRow, TableCell,Grid, TableHead, TableBody, Button } from '@material-ui/core'
import React, {useState, useEffect} from 'react'
import axios from 'axios'
import StaticVar from "./../Config/StaticVar"
import _ from 'lodash'
import ttd from "./../ttd_msh.png"

export default function ReportResult(props) {
    const [result, setresult] = useState([])
    const params = new URLSearchParams(window.location.search);
    const participant_id = params.get("participant_id");

    useEffect(()=>{
        axios.get(StaticVar.URL_API+'/transcript?event_id='+StaticVar.EVENT_ID+'&participant_id='+participant_id+'&dojo_id=0').then(res=>{
            setresult(res.data)
        })
    }, [])

    let total_kihon = (_.sumBy(result.filter(x=>x.CATEGORY==="KIHON"), "SCORE") / result.filter(x=>x.CATEGORY==="KIHON").length).toFixed(2)
    let nilai_kihon = _.sumBy(result.filter(x=>x.CATEGORY==="KIHON"), "SCORE") / result.filter(x=>x.CATEGORY==="KIHON").length * 0.5

    let total_kata = (_.sumBy(result.filter(x=>x.CATEGORY==="KATA"), "SCORE") / result.filter(x=>x.CATEGORY==="KATA").length).toFixed(2)
    let nilai_kata = _.sumBy(result.filter(x=>x.CATEGORY==="KATA"), "SCORE") / result.filter(x=>x.CATEGORY==="KATA").length * 0.25

    let total_kumite = (_.sumBy(result.filter(x=>x.CATEGORY==="KUMITE"), "SCORE") / result.filter(x=>x.CATEGORY==="KUMITE").length).toFixed(2)
    let nilai_kumite = _.sumBy(result.filter(x=>x.CATEGORY==="KUMITE"), "SCORE") / result.filter(x=>x.CATEGORY==="KUMITE").length * 0.25
    return (
        <div style={{padding:20}}>
            <center>
                <Typography>PENGURUS PROVINSI LEMKARI JAWA BARAT</Typography>
                <Typography>UJIAN KENAIKAN TINGKATAN CABANG KHUSUS PUSDIKAJEN</Typography>
                <Typography>TAHUN 2022 SEMESTER I (SATU)</Typography>
                <hr/>
                <Typography>LEMBAR HASIL PENILAIAN</Typography>
            </center>
            <div className="btn_cetak">
                <Button variant="contained" color="primary" onClick={()=>window.print()}>Cetak</Button>
                <Button variant="contained" style={{marginLeft:5}} color="primary" onClick={()=>props.history.goBack()}>Kembali</Button>
            </div>
            
            <Grid container spacing="5" style={{marginTop:10}}>
                <Grid item xs="6">
                    <Table size="small">
                        <TableRow>
                            <TableCell style={{width:80, fontSize:12, padding:4}}>Nama</TableCell>
                            <TableCell style={{width:3, fontSize:12, padding:4}}>:</TableCell>
                            <TableCell style={{fontSize:12, padding:4}}>{result.length>0?result[0].PARTICIPANT_NAME: ""}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{width:80, fontSize:12, padding:4}}>Nomor Induk</TableCell>
                            <TableCell style={{width:3, fontSize:12, padding:4}}>:</TableCell>
                            <TableCell style={{fontSize:12, padding:4}}>{result.length>0?result[0].PARTICIPANT_NUMBER: ""}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{width:80, fontSize:12, padding:4}}>Sabuk / Kyu</TableCell>
                            <TableCell style={{width:3, fontSize:12, padding:4}}>:</TableCell>
                            <TableCell style={{fontSize:12, padding:4}}>{result.length>0?result[0].BELT_NAME: ""} / KYU {result.length>0?result[0].KYU: ""}</TableCell>
                        </TableRow>
                    </Table>
                </Grid>
                <Grid item xs="6">
                    <Table size="small">
                        <TableRow>
                            <TableCell style={{width:80, fontSize:12, padding:4}}>Dojo</TableCell>
                            <TableCell style={{width:3, fontSize:12, padding:4}}>:</TableCell>
                            <TableCell style={{fontSize:12, padding:4}}>{result.length>0?result[0].DOJO_NAME: ""}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{width:80, fontSize:12, padding:4}}>Cabang</TableCell>
                            <TableCell style={{width:3, fontSize:12, padding:4}}>:</TableCell>
                            <TableCell style={{fontSize:12, padding:4}}>{result.length>0?result[0].REGION_NAME: ""}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell style={{width:80, fontSize:12, padding:4}}>Provinsi</TableCell>
                            <TableCell style={{width:3, fontSize:12, padding:4}}>:</TableCell>
                            <TableCell style={{fontSize:12, padding:4}}>JAWA BARAT</TableCell>
                        </TableRow>
                    </Table>
                </Grid>
            </Grid>
            <Grid container style={{marginTop:10}}>
                <Grid item xs="3" style={{textAlign:'center', backgroundColor:"#CFCFCF"}}>KIHON</Grid>
                <Grid item xs="3" style={{textAlign:'center', backgroundColor:"#CFCFCF"}}>KATA</Grid>
                <Grid item xs="3" style={{textAlign:'center', backgroundColor:"#CFCFCF"}}>KUMITE</Grid>
                <Grid item xs="3" style={{textAlign:'center', backgroundColor:"#CFCFCF"}}>NILAI</Grid>

                <Grid item xs="3" style={{textAlign:'center', border:'1px solid #CFCFCF'}}>
                    <Typography style={{fontSize:30}}>{total_kihon}</Typography>
                    <small style={{fontSize:9}}>50% (Bobot) x {total_kihon} =  {nilai_kihon.toFixed(2)}</small>
                </Grid>
                <Grid item xs="3" style={{textAlign:'center', border:'1px solid #CFCFCF'}}>
                    <Typography style={{fontSize:30}}>{total_kata}</Typography>
                    <small style={{fontSize:9}}>25% (Bobot) x {total_kata} =  {nilai_kata}</small>
                </Grid>
                <Grid item xs="3" style={{textAlign:'center', border:'1px solid #CFCFCF'}}>
                    <Typography style={{fontSize:30}}>{total_kumite}</Typography>
                    <small style={{fontSize:9}}>25% (Bobot) x {total_kumite} =  {nilai_kumite}</small>
                </Grid>
                <Grid item xs="3" style={{textAlign:'center', border:'1px solid #CFCFCF'}}>
                    <Typography style={{fontSize:30}}>{(nilai_kihon+nilai_kata+nilai_kumite).toFixed(2)}</Typography>
                    <small style={{fontSize:9}}>{nilai_kihon.toFixed(2)} (KIHON) + {nilai_kata.toFixed(2)} (KATA) + {nilai_kumite.toFixed(2)} (KUMITE) = {(nilai_kihon+nilai_kata+nilai_kumite).toFixed(2)}</small>
                </Grid>
            </Grid>
            <Table size="small" style={{marginTop:10}}>
                <TableHead style={{backgroundColor:"#CFCFCF"}}>
                    <TableRow>
                        <TableCell style={{color:'black',width:10, textAlign:'center'}}>No</TableCell>
                        <TableCell style={{color:'black'}}>Materi</TableCell>
                        <TableCell style={{color:'black',width:100, textAlign:'center'}}>Kategori</TableCell>
                        <TableCell style={{color:'black',width:60, textAlign:'center'}}>Angka</TableCell>
                        <TableCell style={{color:'black',width:200, textAlign:'center'}}>Keterangan</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        result.map((itemnilai,index)=>{
                            let keterangan = ""
                            if(itemnilai.SCORE == 100){keterangan = "SANGAT BAIK"}
                            if(itemnilai.SCORE == 80){keterangan = "BAIK"}
                            if(itemnilai.SCORE == 60){keterangan = "CUKUP"}
                            if(itemnilai.SCORE == 40){keterangan = "KURANG CUKUP"}
                            if(itemnilai.SCORE == 20){keterangan = "KURANG SEKALI"}
                            return(
                                <TableRow style={{padding:0}}>
                                    <TableCell style={{color:'black',width:10, textAlign:'center'}}>{index+1}</TableCell>
                                    <TableCell style={{color:'black'}}>{itemnilai.ASSESMENT_NAME}</TableCell>
                                    <TableCell style={{color:'black',width:100, textAlign:'center'}}>{itemnilai.CATEGORY}</TableCell>
                                    <TableCell style={{color:'black',width:60, textAlign:'center'}}>{itemnilai.SCORE}</TableCell>
                                    <TableCell style={{color:'black',width:200, textAlign:'center'}}>
                                        {
                                            keterangan
                                        }
                                    </TableCell>
                                </TableRow>
                            )
                        })
                    }                    
                </TableBody>
            </Table>
            <center>
                <div style={{width:300, marginTop:20}}>
                    Bekasi, 13 Februari 2022
                    <img src={ttd} style={{width:'80%'}} />
                    <br/>
                    (Djemi Lalisang,SH.,MH.)
                    <br/>
                    KETUA MAJELIS SABUK HITAM <br/>PROVINSI JAWA BARAT
                </div>
                
            </center>
        </div>
    )
}
