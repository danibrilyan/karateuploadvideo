import React, {useState, useEffect} from 'react'
import {Button, AppBar, Toolbar, Grid, Typography, IconButton, Table, TableHead, TableRow, TableCell, TableBody, Checkbox} from '@material-ui/core'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Header from './Components/Header'
import StaticVar from "./../Config/StaticVar"
import axios from 'axios'

export default function Dojo(props) {
    const [dojolist, setdojolist] = useState([])
    useEffect(() => {        
        if(localStorage.user){
            const user = JSON.parse(localStorage.user)
            axios.get(StaticVar.URL_API+'/penguji/dojo?event_id='+StaticVar.EVENT_ID+'&evaluator_id='+user.EVALUATOR_ID).then(res=>{
                console.log('penguji', res.data)
                setdojolist(res.data)
            })
        }
        else{
            axios.get(StaticVar.URL_API+'/peserta/dojo?event_id='+StaticVar.EVENT_ID).then(res=>setdojolist(res.data))
        }
       
    }, [])
    return (
        <div>
            <Header/>
            <div style={{marginTop:10, flex:1, padding:5}}>
                <Grid container spacing={2}>
                    {
                        dojolist.map(item=>{
                            return(
                                <Grid item md={3}>
                                    <Card >
                                        <CardActionArea>
                                            <CardContent>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                {item.DOJO_NAME}
                                            </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                        
                                        <CardActions>                                                
                                            <Button style={{flex:1}} variant="contained" size="small" color="primary" onClick={()=>props.history.push('/app?dojo_name='+item.DOJO_NAME)}>
                                                Lihat
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            )
                        })
                    }
                    
                </Grid>
                
            </div>
        </div>
    )
}
