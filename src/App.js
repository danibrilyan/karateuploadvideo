import React from 'react'
import {
  Route,
  Redirect,
  Switch,
  BrowserRouter as Router,
} from 'react-router-dom';
import Layout from './Layout'

function LayoutRoute({ component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) =>
        (React.createElement(component, props))
      }
    />
  );
}

function App() {
  return (
    <Router>
      <Switch>
          <Route exact path='/' render={() => <Redirect to='/app' />} />
          <LayoutRoute path='/' component={Layout} />
      </Switch>
    </Router>
  );
}

export default App;
