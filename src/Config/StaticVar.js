const BASE_URL_API = 'http://202.83.123.214' //
const PORT_EXAM = '8080'
const GDRIVE = '200'
const KEY_FOLDER = '1WWcYti--6X7qqXyB0R5KNm3kMtOUM_eH'
const event_id = 'EVENT2022020519561210206743'

export default {
	URL_API: `${BASE_URL_API}:${PORT_EXAM}`,
	URI_GDRIVE : `${BASE_URL_API}:${GDRIVE}`,
	KEY_FOLDER : KEY_FOLDER,
	EVENT_ID: event_id
};
