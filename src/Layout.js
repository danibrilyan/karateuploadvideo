import React from "react";
import {
  Route,
  Switch,
  withRouter,
} from "react-router-dom";
import Home from "./Pages/Home";
import Dojo from "./Pages/Dojo";
import Penilaian from "./Pages/Penilaian";
import VideoGrouping from "./Pages/VideoGrouping";
import TranscriptNilai from "./Pages/TranscriptNilai";
import ReportResult from "./Pages/ReportResult";

function Layout() {

  return (
    <>
        <Switch> 
            <Route path="/app" component={Home} />
            <Route path="/dojo" component={Dojo} />
            <Route path="/penilaian" component={Penilaian} />
            <Route path="/hasil" component={TranscriptNilai} />
            <Route path="/reportresult" component={ReportResult} />
            <Route path="/video" component={VideoGrouping} />
            {/* <Route path="/dana/auth/callback" component={Dana} />         */}
        </Switch>
    </>
  );
}

export default withRouter(Layout);
